// flow-typed signature: 615ac39a859458ec7cfd64e1feb0327d
// flow-typed version: <<STUB>>/babel-preset-async-to-bluebird_v1.1.0/flow_v0.36.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'babel-preset-async-to-bluebird'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'babel-preset-async-to-bluebird' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'babel-preset-async-to-bluebird/index' {
  declare module.exports: $Exports<'babel-preset-async-to-bluebird'>;
}
declare module 'babel-preset-async-to-bluebird/index.js' {
  declare module.exports: $Exports<'babel-preset-async-to-bluebird'>;
}
