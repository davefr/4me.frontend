// flow-typed signature: 251d8016938a0eb17511ed77b2204a91
// flow-typed version: <<STUB>>/autoprefixer-loader_v3.2.0/flow_v0.36.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'autoprefixer-loader'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'autoprefixer-loader' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'autoprefixer-loader/index' {
  declare module.exports: $Exports<'autoprefixer-loader'>;
}
declare module 'autoprefixer-loader/index.js' {
  declare module.exports: $Exports<'autoprefixer-loader'>;
}
